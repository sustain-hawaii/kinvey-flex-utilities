import { Db, appConfig, log } from '@sustainhawaii/heal-common'
import rp from 'request-promise'
import moment from 'moment'
import _ from 'lodash'

/**
 * Initializes Kinvey Flex SDK with custom endpoint and data handlers.
 *
 * @param {object=} functionHandlers - object with function names as keys, and flex functions as values
 * @param {object=} dataHandlers - object with service object names as keys, and objects as values;
 * @param {function[]=} serviceInitializers - array of functions to call after registering flex functions and service objects
 *    objects have data event names as keys, and event handler functions as values
 */
export const initFlexSdk = ({
  functionHandlers,
  dataHandlers,
  serviceInitializers
}) => {
  log(`Initializing Flex SDK`)
  log(`APP_ENV = "${process.env.APP_ENV}"`)
  const sdk = require('kinvey-flex-sdk')
  const opts = {}
  if (process.env.PORT) {
    opts.port = process.env.PORT
  }
  sdk.service(opts, (err, flex) => {
    if (err) {
      log('error initializing Flex service:', err)
    } else {
      const nodeVersion = process.version
      log(`Node.js version: ${nodeVersion}`)
      if (functionHandlers) {
        _.forEach(functionHandlers, (func, name) => {
          flex.functions.register(name, func)
        })
      }
      if (dataHandlers) {
        _.forEach(dataHandlers, (eventConfigs, serviceObjectName) => {
          const serviceObject = flex.data.serviceObject(serviceObjectName)
          _.forEach(eventConfigs, (eventHandler, eventName) => {
            serviceObject[eventName](eventHandler)
          })
        })
      }
      log(`Initialized Flex service.\n\nFlex SDK version ${flex.version}`)
    }
  })
  if (serviceInitializers) {
    _.forEach(serviceInitializers, initializer => initializer())
  }
}

export const getEnvironmentConfig = modules => {
  const { environments } = appConfig
  const appKey = modules && modules.backendContext.getAppKey()
  return _.find(environments, env => env.appKey === appKey)
}

export const getPwaUrl = modules => {
  const { pwaDomain } = appConfig
  const envConfig = getEnvironmentConfig(modules)
  return envConfig.pwaUrl.replace('DOMAIN', pwaDomain)
}

export const getSecurityContext = modules => modules.requestContext.getSecurityContext()

export const inMasterSecurityContext = modules => getSecurityContext(modules) === 'master'

export const calledByWebhookUser = ({ userId }, modules) => {
  const envConfig = getEnvironmentConfig(modules)
  return userId === envConfig.webhooksUserId
}

/**
 * Return true if user is a sys-admin
 * @param {string} userId - userId from context param of custom endpoint
 * @param {object} modules
 * @return {Promise<boolean>}
 */
export const calledBySysAdmin = async ({ userId }, modules) => {
  try {
    const user = await Db.getUser(userId, modules)
    return _.get(user, 'adminLevel') === 'sys'
  } catch (e) {
    return false
  }
}

/**
 * Complete a flex function or data handler with a runtimeError
 *
 * @param {string} callerName
 * @param {function} complete
 * @param {object} error
 * @return {*}
 */
export const completeWithError = (callerName, complete, error) => {
  if (_.isError(error)) {
    log(`${callerName} error:`)
    console.log(error)
  } else {
    log(`${callerName} error:`, error)
  }
  return complete().setBody(error).runtimeError().done()
}

/**
 * Complete a flex function or data handler with a body and ok().next()
 *
 * @param {string} callerName
 * @param {function} complete
 * @param {object} body
 * @param {object=} startMoment - instance of moment.js
 * @return {*}
 */
export const completeWithBody = (callerName, complete, body, startMoment) => {
  const timeStr = startMoment ? ` in ${moment().diff(startMoment, 'seconds')} seconds` : ''
  log(`${callerName} completed${timeStr}`)
  if (body) {
    return complete().setBody(body).ok().next()
  } else {
    return complete().ok().next()
  }
}

export const executeCustomEndpoint = ({
  name,
  body,
  opts = {},
  modules
}) => {
  const runner = modules.endpointRunner(opts).endpoint(name)
  return runner.execute(body)
}

/**
 * Send POST request to REST API for custom endpoint with master credentials.
 *
 * @param {string} name - name of custom endpoint
 * @param {string} masterSecret - Kinvey App Secret of target environment. (Cannot be stored in this module because public.)
 * @param params
 */
export const executeCustomEndpointAsMasterREST = ({
  name,
  masterSecret,
  params
}) => {
  const appKey = _.get(appConfig, `environments.${process.env.APP_ENV}.appKey`)
  return rp({
    method: 'POST',
    uri: `https://kvy-us2-baas.kinvey.com/rpc/${appKey}/custom/${name}`,
    auth: {
      username: appKey,
      password: masterSecret
    },
    json: true,
    body: params
  })
}

/**
 * Custom endpoint template used to try out various functions in a service.
 *
 * Usage:
 * 1. Import this function into a file in a flex service.
 * 2. Set an array const 'tests' whose keys are test names and values test functions.
 * 3. When exporting all flex functions, export this custom endpoint with a service-specific name, as in this example:
 * export { default as testServiceHRA } from './testingCustomEndpoint'
 '
 *
 * @param {object} tests - keys are test names, values are test functions
 * @param {object} context
 * @param {object} context.body
 * @param {string} context.body.name - name of test corresponding to exported function in this file
 * @param {object} context.body.params - params to pass to test function
 * @param complete
 * @param modules
 * @return {Promise<object>}
 */
export const testingCustomEndpoint = async ({
  tests,
  context: {
    body: {
      name,
      params
    }
  },
  complete,
  modules,
}) => {
  const endpointName = 'testingCustomEndpoint'
  try {
    if (!inMasterSecurityContext(modules)) {
      throw new Error('invalid credentials')
    }
    if (!name || typeof tests[name] !== 'function') {
      throw new Error('invalid test name')
    }
    log(`${endpointName} starting test "${name}"`, undefined, params.debug)
    const results = await tests[name]({
      ...params,
      modules
    })
    log(`${endpointName} completed test "${name}"`, undefined, params.debug)
    return completeWithBody(endpointName, complete, results)
  } catch (e) {
    return completeWithError(endpointName, complete, e)
  }
}

/**
 * Send a message using the sendMessage custom endpoint.
 *
 * @param {object} message
 * @param {string} message.toUserId - Kinvey user id
 * @param {string} message.type - 'notice', 'alert', 'action'
 * @param {string=} message.urgency - 'info' (default), 'success', 'warn', 'error'
 * @param {string|string[]} message.message - body of message
 * @param {string=} message.title - short title for alert or notification; defaults to "Alert" for alert dialog
 * @param {number} message.duration - milliseconds until notification is dismissed, or 0 for never; defaults to 5000
 * @param {object[]=} message.actions - array of action objects representing button(s) to display on notices.
 * @param {string=} message.actions[].label - label for notice action button
 * @param {string=} message.actions[].path - path of PWA route to navigate to when button clicked
 * @param {object} modules - Kinvey modules
 * @param {object=} opts - options for modules.endpointRunner; see https://devcenter.kinvey.com/nodejs/reference/flex/reference.html#endpoint-runner-module
 * @return {Promise}
 */
export const sendMessage = (message, modules, opts) => {
  return executeCustomEndpoint({
    name: 'sendMessage',
    body: message,
    opts,
    modules
  })
}
