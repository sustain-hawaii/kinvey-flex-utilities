# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.23](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.22...v1.2.23) (2020-04-01)

### [1.2.22](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.21...v1.2.22) (2020-03-28)

### [1.2.21](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.20...v1.2.21) (2020-03-28)

### [1.2.20](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.19...v1.2.20) (2020-03-28)

### [1.2.19](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.18...v1.2.19) (2020-03-27)

### [1.2.18](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.17...v1.2.18) (2020-03-27)

### [1.2.17](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.16...v1.2.17) (2020-03-26)

### [1.2.16](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.15...v1.2.16) (2020-03-26)

### [1.2.15](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.14...v1.2.15) (2020-03-26)

### [1.2.14](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.13...v1.2.14) (2020-03-23)

### [1.2.13](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.12...v1.2.13) (2020-03-22)

### [1.2.12](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.11...v1.2.12) (2020-03-22)

### [1.2.11](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.10...v1.2.11) (2020-03-22)

### [1.2.10](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.9...v1.2.10) (2020-03-20)

### [1.2.9](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.8...v1.2.9) (2020-03-18)

### [1.2.8](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.7...v1.2.8) (2020-03-16)

### [1.2.7](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.6...v1.2.7) (2020-03-16)

### [1.2.6](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.5...v1.2.6) (2020-03-16)

### [1.2.5](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.4...v1.2.5) (2020-03-16)

### [1.2.4](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.3...v1.2.4) (2020-03-16)

### [1.2.3](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.2...v1.2.3) (2020-03-16)

### [1.2.2](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.1...v1.2.2) (2020-03-16)

### [1.2.1](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.2.0...v1.2.1) (2020-03-16)

## [1.2.0](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.12...v1.2.0) (2020-03-16)


### Features

* testingCustomEndpoint ([b6cbeb3](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/commit/b6cbeb3c2a67f6735d1d25b3c8701f1fb6e47901))

### [1.1.12](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.11...v1.1.12) (2020-03-09)

### [1.1.11](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.10...v1.1.11) (2020-02-29)

### [1.1.10](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.9...v1.1.10) (2020-02-29)

### [1.1.9](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.8...v1.1.9) (2020-02-23)

### [1.1.8](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.7...v1.1.8) (2020-02-23)

### [1.1.7](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.6...v1.1.7) (2020-02-23)

### [1.1.6](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.5...v1.1.6) (2020-02-22)

### [1.1.5](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.4...v1.1.5) (2020-02-20)

### [1.1.4](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.3...v1.1.4) (2020-02-20)

### [1.1.3](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.2...v1.1.3) (2020-02-20)

### [1.1.2](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.1...v1.1.2) (2020-02-20)

### [1.1.1](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.1.0...v1.1.1) (2020-02-20)

## [1.1.0](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.8...v1.1.0) (2020-02-20)


### Features

* add option to display completion time in completeWithBody ([368d9ef](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/commit/368d9ef5ffc44eb20ca3e6e55341d9e5e51c2bec))

### [1.0.8](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.7...v1.0.8) (2020-02-18)

### [1.0.7](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.6...v1.0.7) (2020-02-15)

### [1.0.6](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.5...v1.0.6) (2020-02-14)

### [1.0.5](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.4...v1.0.5) (2020-02-14)

### [1.0.4](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.3...v1.0.4) (2020-02-14)

### [1.0.3](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.2...v1.0.3) (2020-02-07)


### Bug Fixes

* log complete error with trace in completeWithError ([77e3230](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/commit/77e3230d6a9b3a7996ff73a83c9942e396c57f1b))

### [1.0.2](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/compare/v1.0.1...v1.0.2) (2020-01-04)

### 1.0.1 (2020-01-04)


### Bug Fixes

* remove unused method ([ab8a107](https://bitbucket.org/sustain-hawaii/kinvey-flex-utilities/commit/ab8a1071a12f956f65248b6e8f2bfdb04b006460))
